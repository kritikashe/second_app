require 'test_helper'

class UserUpdatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_update = user_updates(:one)
  end

  test "should get index" do
    get user_updates_url
    assert_response :success
  end

  test "should get new" do
    get new_user_update_url
    assert_response :success
  end

  test "should create user_update" do
    assert_difference('UserUpdate.count') do
      post user_updates_url, params: { user_update: { content: @user_update.content, title: @user_update.title, user_id: @user_update.user_id } }
    end

    assert_redirected_to user_update_url(UserUpdate.last)
  end

  test "should show user_update" do
    get user_update_url(@user_update)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_update_url(@user_update)
    assert_response :success
  end

  test "should update user_update" do
    patch user_update_url(@user_update), params: { user_update: { content: @user_update.content, title: @user_update.title, user_id: @user_update.user_id } }
    assert_redirected_to user_update_url(@user_update)
  end

  test "should destroy user_update" do
    assert_difference('UserUpdate.count', -1) do
      delete user_update_url(@user_update)
    end

    assert_redirected_to user_updates_url
  end
end
