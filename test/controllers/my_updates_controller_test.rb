require 'test_helper'

class MyUpdatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @my_update = my_updates(:one)
  end

  test "should get index" do
    get my_updates_url
    assert_response :success
  end

  test "should get new" do
    get new_my_update_url
    assert_response :success
  end

  test "should create my_update" do
    assert_difference('MyUpdate.count') do
      post my_updates_url, params: { my_update: { content: @my_update.content, title: @my_update.title, user_id: @my_update.user_id } }
    end

    assert_redirected_to my_update_url(MyUpdate.last)
  end

  test "should show my_update" do
    get my_update_url(@my_update)
    assert_response :success
  end

  test "should get edit" do
    get edit_my_update_url(@my_update)
    assert_response :success
  end

  test "should update my_update" do
    patch my_update_url(@my_update), params: { my_update: { content: @my_update.content, title: @my_update.title, user_id: @my_update.user_id } }
    assert_redirected_to my_update_url(@my_update)
  end

  test "should destroy my_update" do
    assert_difference('MyUpdate.count', -1) do
      delete my_update_url(@my_update)
    end

    assert_redirected_to my_updates_url
  end
end
