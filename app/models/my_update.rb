class MyUpdate < ApplicationRecord
  belongs_to :user
  # attr_accessor :date
  validates :content, presence: true
  validates :title, presence: true

end
