class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :managers, dependent: :destroy
  has_many :my_updates
end

#has_many :manager_user, class_name: 'User', foriegn_key: 'manager_user_id'
#has_many :user, class_name: 'User', foriegn_key: 'user_id'
