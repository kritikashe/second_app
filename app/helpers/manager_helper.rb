module ManagerHelper

	def mangers_email manager_id
		manager = User.find_by_id(manager_id)
		if manager.present?
			email = manager.email
		else
			email = ""
		end
		email
	end
end
