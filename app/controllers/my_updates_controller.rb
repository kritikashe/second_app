class MyUpdatesController < ApplicationController
  before_action :set_my_update, only: [:edit, :update]

  # GET /my_updates
  # GET /my_updates.json
  def index
    if params[:id].present?
      @my_updates = MyUpdate.where(user_id: params[:id]).order('created_at DESC')
      @type="id"
    else 
      if params[:search][:date].present?
      @type="date"
      if params[:search][:idt].present?
      @my_updates = MyUpdate.where(date: params[:search][:date], user_id: params[:search][:idt].split(' ')).order('created_at DESC')
      #puts @my_updates.count.inspect
      else
      flash[:success] = "Sorry!!!There is no user under you"
      redirect_to  manager_index_path 
      end
      else
        flash[:success] = "no data for this date"
      redirect_to  manager_index_path 
      end
    end
  end
  # GET /my_updates/1
  # GET /my_updates/1.json
  def show
    @my_update = MyUpdate.where(user_id: params[:id])
  end
   
   # GET /my_updates/new
  def new
    @my_update = MyUpdate.new
  end

  # GET /my_updates/1/edit
  def edit
  end

  # POST /my_updates
  # POST /my_updates.json
  def create
    #params[:my_update][:date]
    @my_update = MyUpdate.new(my_update_params)
    @my_update.user_id = current_user.id
    #@my_update.date = params[:date]
    @my_update.save
    respond_to do |format|
      if @my_update.save
        format.html { redirect_to @my_update, notice: 'My update was successfully created.' }
        format.json { render :show, status: :created, location: @my_update }
      else
        format.html { render :new }
        format.json { render json: @my_update.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /my_updates/1
  # PATCH/PUT /my_updates/1.json
  def update
    respond_to do |format|
      if @my_update.update(my_update_params)
        format.html { redirect_to @my_update, notice: 'My update was successfully updated.' }
        format.json { render :show, status: :ok, location: @my_update }
      else
        format.html { render :edit }
        format.json { render json: @my_update.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /my_updates/1
  # DELETE /my_updates/1.json
  def destroy
    @my_update.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_my_update
      @my_update = MyUpdate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def my_update_params
        # my=MyUpdate.new
        # my.user_id =current_user.id
      params.require(:my_update).permit(:title, :content, :user_id, :date)
    end
end
