json.extract! my_update, :id, :title, :content, :user_id, :created_at, :updated_at
json.url my_update_url(my_update, format: :json)
