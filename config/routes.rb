Rails.application.routes.draw do
  resources :my_updates
  devise_for :users
  get 'static_pages/home'
  resources :users
  resources :manager
  get '/dashboard',to: 'manager#done'
  get '/action_page.php', to: 'manager#create'
  get 'static_pages/help'
  root 'static_pages#home'
  get  'static_pages/about'
  get  '/signup',  to: 'users#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
