class CreateManagerUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :manager_users do |t|
     t.integer :manager_id
     t.integer :user_id
      t.timestamps
    end
  end
end
