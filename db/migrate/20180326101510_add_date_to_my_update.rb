class AddDateToMyUpdate < ActiveRecord::Migration[5.1]
  def change
    add_column :my_updates, :date, :string
    add_column :my_updates, :datetime, :string
  end
end
