class CreateManagers < ActiveRecord::Migration[5.1]
  def change
    create_table :managers do |t|
      t.references :user, foreign_key: true
      t.integer :manager_user_id
      t.timestamps
    end
  end
end
